
// We're just using "printf" for debugging, so don't warn us about console safety.
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <Windows.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>

#include <SFML/Graphics.hpp>

// A simple demo app of getting "mimimalist" asteroids working,
// where I use the smallest possible API:
//   1) Clearing the screen = window.clear();
//   2) Drawing a line = window.draw(some_data, sf::Lines).
//   3) Time = timer.getElapsedTime().asMilliseconds();

// We'll roll our own translations, rotations, and collision detection.
// The "asteroids" are simple squares that bounce off each other.
// Also, we're going to keep it simple and do all of this in one file.

// Other simplifications:
// The game will always run at the default window size.
// Also, no sound effects, or particle effects, etc.
// This isn't meant to be pretty, just functional.

// Got the "ship death" and "game over" working, and I believe that's it! Let's move on.
// -- Alan Daniels, Sept 25th, 2016.


// I'll forgive a couple of expedient globals here.
int g_screen_width  = 0;
int g_screen_height = 0;


// Game constants (expressed in "per second", translated to "per frame").
const int FRAMES_PER_SEC  = 50;
const int MSECS_PER_FRAME = 1000 / FRAMES_PER_SEC;

const float SHIP_RADIUS = 15.0f;

const float SHIP_THRUST_PER_FRAME     =  20.0f / FRAMES_PER_SEC;
const float SHIP_MAX_SPEED_PER_FRAME  = 500.0f / FRAMES_PER_SEC;
const float SHIP_ROT_PER_FRAME        = 360.0f / FRAMES_PER_SEC;
const int   SHIP_FRAMES_BETWEEN_SHOTS = static_cast<int>(0.25 * FRAMES_PER_SEC);

const int   ASTEROID_INITIAL_COUNT = 8;
const float AST_RADIUS_SMALL  = 10.0f;
const float AST_RADIUS_MEDIUM = 30.0f;
const float AST_RADIUS_LARGE  = 50.0f;

const float AST_SPEED_PER_FRAME_SMALL  = 90.0f / FRAMES_PER_SEC;
const float AST_SPEED_PER_FRAME_MEDIUM = 70.0f / FRAMES_PER_SEC;
const float AST_SPEED_PER_FRAME_LARGE  = 50.0f / FRAMES_PER_SEC;

const float BULLET_RADIUS = 2;
const int   BULLET_MAX_COUNT = 5;
const float BULLET_SPEED_PER_FRAME = 500.0 / FRAMES_PER_SEC;
const int   BULLET_LIFETIME_FRAMES = 3 * FRAMES_PER_SEC;


// Headers.

struct Vec2 {
	Vec2() : x(0), y(0) {}
	Vec2(const Vec2 &that) : x(that.x), y(that.y) {}
	Vec2(float xarg, float yarg) : x(xarg), y(yarg) {}

	Vec2 &operator=(const Vec2 &that);
	Vec2 getUnit() const;

	Vec2 reversed() const { return Vec2(-x, -y); }
	float Vec2::getLength() const { return sqrt((x * x) + (y * y)); }

	static Vec2 Add(const Vec2 &one, const Vec2 &two);
	static Vec2 Subtract(const Vec2 &one, const Vec2 &two);
	static Vec2 Multiply(const Vec2 &vec, float value);
	static Vec2 Rotate(const Vec2 &vec, float degrees);
	static float CrossProduct(const Vec2 &one, const Vec2 &two);

	float x;
	float y;
};


class BoundingBox {
public:
	BoundingBox();
	BoundingBox(float radius, const Vec2 &center);
	BoundingBox &operator=(const BoundingBox &that);

	float getRadius() const { return m_radius; }
	const Vec2 getCenter() const { return m_center; }

	float getLeft()   const { return m_center.x - m_radius; }
	float getRight()  const { return m_center.x + m_radius; }
	float getTop()    const { return m_center.y - m_radius; }
	float getBottom() const { return m_center.y + m_radius; }

	bool intersectionTest(const BoundingBox &other) const;

	void applyMomentum(const Vec2 &momentum);
	void draw(sf::RenderWindow &win) const;

private:
	float m_radius;
	Vec2  m_center;
	Vec2  m_initial_pts[4];
	Vec2  m_outline_pts[4];
};


class Game;

class Ship {
public:
	Ship(Game *pGame);
	void resetToCenter();
	void update();
	void draw(sf::RenderWindow &win);

	Vec2  getTipPoint() const { return m_outline_pts[2]; }
	Vec2  getPosition() const { return m_position; }
	float getRotation() const { return m_rotation; }
	Vec2  getMomentum() const { return m_momentum; }
	float getSpeed()    const { return m_momentum.getLength() * FRAMES_PER_SEC; }
	Vec2  getTipDir()   const {	return (Vec2::Rotate(m_initial_pts[2], m_rotation)).getUnit(); }

	BoundingBox getBBox() const { return m_bbox; }

private:
	Game *m_pGame;

	Vec2  m_initial_pts[5];
	Vec2  m_outline_pts[5];

	Vec2  m_momentum;
	Vec2  m_position;
	float m_rotation;

	BoundingBox m_bbox;

	void rebuildOutline();
};


class Bullet {
public:
	Bullet(const Vec2 &position, const Vec2 &momentum);
	Bullet(const Bullet &that);

	void update();
	void draw(sf::RenderWindow &win) const { m_bbox.draw(win); }

	void markAsDead() { m_alive = false; }
	bool isAlive() const { return m_alive; }

	bool isExpired() const { return (m_frames_left <= 0); }
	const BoundingBox &getBbox() const { return m_bbox;  }
	const Vec2 &getMomentum() const { return m_momentum; }

private:
	Bullet(); // Disallow the default ctor.

	int m_frames_left;
	bool m_alive;
	BoundingBox m_bbox;
	Vec2 m_momentum;
};


enum ASTEROID_SIZE { AST_SIZE_SMALL, AST_SIZE_MEDIUM, AST_SIZE_LARGE };

class Asteroid {
public:
	Asteroid(Game *pGame, ASTEROID_SIZE size, const Vec2 &position, const Vec2 &momentum);

	void markAsDead() { m_alive = false; }
	bool isAlive() const { return m_alive; }

	void update() { m_bbox.applyMomentum(m_momentum); }
	void draw(sf::RenderWindow &win) const { m_bbox.draw(win); }

	void setMomentum(const Vec2 &val) { m_momentum = val; }
	const Vec2 &getMomentum() const { return m_momentum; }

	ASTEROID_SIZE getSize() const { return m_size; }
	const BoundingBox &getBbox() const { return m_bbox; }

private:
	Asteroid(); // Disallow the default ctor.

	Game *m_pGame;
	bool m_alive;
	BoundingBox m_bbox;
	Vec2 m_momentum;
	ASTEROID_SIZE m_size;
};


class Game {
public:
	Game(const sf::Font &font);
	
	void onKeyPressed(sf::Keyboard::Key code);
	void onKeyReleased(sf::Keyboard::Key code);
	void update();
	void draw(sf::RenderWindow &win);

	bool getFlamePressed()  const { return m_flame_pressed; }
	bool getShootPressed()  const { return m_shoot_pressed; }
	bool getRotCWPressed()  const { return m_rot_cw_pressed; }
	bool getRotCCWPressed() const { return m_rot_ccw_pressed; }
	int getScore() const { return m_score; }
	int getLivesLeft() const { return m_lives_left; }

	Ship &getShip() { return m_ship; }

	void addToScore(int val) { m_score += val; }

	void handleShipDeath();

private:
	// Disable the default ctor. We always need the font passed in.
	Game() : m_ship(this) {}

	enum PLAY_STATE {
		NORMAL,
		SHIP_DEATH,
		GAME_OVER
	};

	Ship m_ship;
	std::vector<Bullet> m_bullets;
	std::vector<Asteroid> m_asteroids;

	sf::Text m_score_text;
	sf::Text m_big_text;

	Vec2 m_spare_one[4];
	Vec2 m_spare_two[4];

	int  m_frames_until_next_shot;
	bool m_flame_pressed;
	bool m_shoot_pressed;
	bool m_rot_cw_pressed;
	bool m_rot_ccw_pressed;
	int  m_score;
	int  m_lives_left;

	PLAY_STATE m_play_state;
	int m_special_timer;

	void spawnAllAsteroids();
	void spawnBullet();
	void spawnBabyAsteroids(const Bullet &bullet, const Asteroid &asteroid, std::vector<Asteroid> *results);
	void bounceAsteroids(Asteroid &one, Asteroid &two);

	void updateNormal();
	void updateSpecial();

	void drawNormal(sf::RenderWindow &win);
	void drawShipDeath(sf::RenderWindow &win);
	void drawGameOver(sf::RenderWindow &win);

	void drawLivesLeft(sf::RenderWindow &win);
	void drawScoreText(sf::RenderWindow &win);
	void drawBigText(sf::RenderWindow &win, const char *msg);
};


// Print a debug message.
void PrintDebug(const char *msg) {
	printf(msg);
	OutputDebugStringA(msg);
}


// Print out all the possible window sizes and bit depths.
// Meh. We only wanted to run this once to see what was out there.
void EnumWindowSizes() {
	auto modes = sf::VideoMode::getFullscreenModes();
	for (auto it = modes.begin(); it != modes.end(); it++) {
		char msg[64];
		sprintf(msg, "Width = %4d, Height = %4d, Bits = %2d\n", it->width, it->height, it->bitsPerPixel);
		PrintDebug(msg);
	}
}


// Draw a single line. In a normal game, we'd optimize this to be done in
// batches, but part of the challenge here is to have a "draw line" primitive.
// Fortunately this just draws the line in white, which is just what we need.
void DrawLine(sf::RenderWindow &win, const Vec2 &one, const Vec2 &two) {
	// Draw the line itself.
	sf::Vertex line_pts[] = {
		sf::Vertex(sf::Vector2f(one.x, one.y)),
		sf::Vertex(sf::Vector2f(two.x, two.y))
	};
	win.draw(line_pts, 2, sf::Lines);

	// Deal with screen wrapping.
	float wrapped_x1 = one.x;
	float wrapped_y1 = one.y;
	float wrapped_x2 = two.x;
	float wrapped_y2 = two.y;
	bool wrap_occurred = false;
		
	// Deal with X underflow.
	if ((wrapped_x1 < 0) || (wrapped_x2 < 0)) {
		wrapped_x1 += g_screen_width;
		wrapped_x2 += g_screen_width;
		wrap_occurred = true;
	}
	else if ((wrapped_x1 >= g_screen_width) || (wrapped_x2 >= g_screen_width)) {
		wrapped_x1 -= g_screen_width;
		wrapped_x2 -= g_screen_width;
		wrap_occurred = true;
	}

	if ((wrapped_y1 < 0) || (wrapped_y2 < 0)) {
		wrapped_y1 += g_screen_height;
		wrapped_y2 += g_screen_height;
		wrap_occurred = true;
	}
	else if ((wrapped_y1 >= g_screen_height) || (wrapped_y2 >= g_screen_height)) {
		wrapped_y1 -= g_screen_height;
		wrapped_y2 -= g_screen_height;
		wrap_occurred = true;
	}

	if (wrap_occurred) {
		sf::Vertex wrapped_pts[] = {
			sf::Vertex(sf::Vector2f(wrapped_x1, wrapped_y1)),
			sf::Vertex(sf::Vector2f(wrapped_x2, wrapped_y2))
		};
		win.draw(wrapped_pts, 2, sf::Lines);
	}
}


// Assignment operator.
Vec2 &Vec2::operator=(const Vec2 &that) {
	x = that.x;
	y = that.y;
	return *this;
}


// Get the unit version.
Vec2 Vec2::getUnit() const {
	float len = getLength();
	float newx = x / len;
	float newy = y / len;
	return Vec2(newx, newy);
}


// Add two vectors together.
Vec2 Vec2::Add(const Vec2 &one, const Vec2 &two) {
	float x = one.x + two.x;
	float y = one.y + two.y;
	return Vec2(x, y);
}


// Subtrace one vector from another.
Vec2 Vec2::Subtract(const Vec2 &one, const Vec2 &two) {
	float x = one.x - two.x;
	float y = one.y - two.y;
	return Vec2(x, y);
}


// Multiply a (unit) vector times a scalar.
Vec2 Vec2::Multiply(const Vec2 &vec, float value) {
	float x = vec.x * value;
	float y = vec.y * value;
	return Vec2(x, y);
}


// Rotate a vector by degrees.
Vec2 Vec2::Rotate(const Vec2 &vec, float degrees) {
	float radians = static_cast<float>((degrees / 180.0f) * M_PI);
	float cos_val = cos(radians);
	float sin_val = sin(radians);

	float x = (vec.x * cos_val) - (vec.y * sin_val);
	float y = (vec.x * sin_val) + (vec.y * cos_val);
	return Vec2(x, y);
}


// Cross product.
float Vec2::CrossProduct(const Vec2 &one, const Vec2 &two) {
	return (one.x * two.y) - (one.y * two.x);
}


// Bounding Box default ctor.
// Just adding this for the sake of well-defined behavoir.
BoundingBox::BoundingBox() {
	m_radius = 0;
	m_center = Vec2(0, 0);

	m_initial_pts[0] = Vec2(0, 0);
	m_initial_pts[1] = Vec2(0, 0);
	m_initial_pts[2] = Vec2(0, 0);
	m_initial_pts[3] = Vec2(0, 0);

	m_outline_pts[0] = Vec2(0, 0);
	m_outline_pts[1] = Vec2(0, 0);
	m_outline_pts[2] = Vec2(0, 0);
	m_outline_pts[3] = Vec2(0, 0);
}


// Bounding Box ctor from values.
BoundingBox::BoundingBox(float radius, const Vec2 &center) {
	m_radius = radius;
	m_center = center;

	m_initial_pts[0] = Vec2(-m_radius, -m_radius);
	m_initial_pts[1] = Vec2( m_radius, -m_radius);
	m_initial_pts[2] = Vec2( m_radius,  m_radius);
	m_initial_pts[3] = Vec2(-m_radius,  m_radius);
	for (int i = 0; i < 4; i++) {
		m_outline_pts[i] = Vec2::Add(m_initial_pts[i], m_center);
	}
}


// Assignment operator.
BoundingBox &BoundingBox::operator=(const BoundingBox &that) {
	m_radius = that.m_radius;
	m_center = that.m_center;
	for (int i = 0; i < 4; i++) {
		m_initial_pts[i] = that.m_initial_pts[i];
	}
	for (int i = 0; i < 4; i++) {
		m_outline_pts[i] = that.m_outline_pts[i];
	}

	return *this;
};


// Do an interesection test.
bool BoundingBox::intersectionTest(const BoundingBox &other) const {
	bool result = (
		(getLeft() <= other.getRight()) &&
		(other.getLeft() <= getRight()) &&
		(getTop() <= other.getBottom()) &&
		(other.getTop() <= getBottom()));
	return result;
}


// Update the center, do any screen wrapping, and rebuild the outline.
void BoundingBox::applyMomentum(const Vec2 &momentum) {
	m_center = Vec2::Add(m_center, momentum);

	if (m_center.x < 0) {
		m_center.x += g_screen_width;
	}
	else if (m_center.x > g_screen_width) {
		m_center.x -= g_screen_width;
	}

	if (m_center.y < 0) {
		m_center.y += g_screen_height;
	}
	else if (m_center.y > g_screen_height) {
		m_center.y -= g_screen_height;
	}

	// Rebuild the outline.
	for (int i = 0; i < 4; i++) {
		m_outline_pts[i] = Vec2::Add(m_initial_pts[i], m_center);
	}
}


// Draw the bounding box.
void BoundingBox::draw(sf::RenderWindow &win) const {
	for (int i = 0; i < 4; i++) {
		const Vec2 &begin = m_outline_pts[i];
		const Vec2 &end = m_outline_pts[(i + 1) % 4];
		DrawLine(win, begin, end);
	}
}


// Default ctor.
Ship::Ship(Game *pGame) {
	m_pGame = pGame;

	m_initial_pts[0] = Vec2(0, 0);                      // Bottom.
	m_initial_pts[1] = Vec2(-SHIP_RADIUS, SHIP_RADIUS); // Left fin.
	m_initial_pts[2] = Vec2(0, -SHIP_RADIUS);           // Tip.
	m_initial_pts[3] = Vec2(SHIP_RADIUS, SHIP_RADIUS);  // Right fin.
	m_initial_pts[4] = Vec2(0, SHIP_RADIUS * 2);        // Flame.

	m_momentum = Vec2(0, 0);
	m_position = Vec2(g_screen_width / 2.0f, g_screen_height / 2.0f);
	m_rotation = 0;

	m_bbox = BoundingBox(SHIP_RADIUS, m_position);
	rebuildOutline();
}


// Reset the ship to the center.
void Ship::resetToCenter() {
	m_momentum = Vec2(0, 0);
	m_position = Vec2(g_screen_width / 2.0f, g_screen_height / 2.0f);
	m_rotation = 0;
	rebuildOutline();
}


// Apply any inputs to the ship's state.
void Ship::update() {
	// Apply any rotation.
	if (m_pGame->getRotCWPressed()) {
		m_rotation += SHIP_ROT_PER_FRAME;
		while (m_rotation > 360.0) {
			m_rotation -= 360.0;
		}
	}
	else if (m_pGame->getRotCCWPressed()) {
		m_rotation -= SHIP_ROT_PER_FRAME;
		while (m_rotation < 0.0) {
			m_rotation += 360.0;
		}
	}

	// If they're accelerating, apply thrust.
	Vec2 positive = Vec2::Rotate(Vec2(0, -1), m_rotation);
	if (m_pGame->getFlamePressed()) {
		m_momentum = Vec2::Add(Vec2::Multiply(positive, SHIP_THRUST_PER_FRAME), m_momentum);
	}

	// Cap the ship's speed.
	float len = m_momentum.getLength();
	if (len > SHIP_MAX_SPEED_PER_FRAME) {
		m_momentum = Vec2::Multiply(m_momentum.getUnit(), SHIP_MAX_SPEED_PER_FRAME);
	}

	// Update the position, and do any screen wrapping.
	m_position = Vec2::Add(m_position, m_momentum);

	if (m_position.x < 0) {
		m_position.x += g_screen_width;
	}
	else if (m_position.x > g_screen_width) {
		m_position.x -= g_screen_width;
	}

	if (m_position.y < 0) {
		m_position.y += g_screen_height;
	}
	else if (m_position.y > g_screen_height) {
		m_position.y -= g_screen_height;
	}

	rebuildOutline();
}


// Rebuild the ship's outline.
void Ship::rebuildOutline() {
	// Rebuild the ships hull points.
	for (int i = 0; i < 5; i++) {
		Vec2 rotated = Vec2::Rotate(m_initial_pts[i], m_rotation);
		m_outline_pts[i] = Vec2::Add(rotated, m_position);
	}

	// Then, rebuilding the bounding box around those points.
	m_bbox = BoundingBox(SHIP_RADIUS, m_position);
}


void Ship::draw(sf::RenderWindow &win) {
	// Points 0 through 3 are for the ship itself.
	for (int i = 0; i < 4; i++) {
		const Vec2 &begin = m_outline_pts[i];
		const Vec2 &end = m_outline_pts[(i + 1) % 4];
		DrawLine(win, begin, end);
	}

	// The last line is for the flame.
	if (m_pGame->getFlamePressed()) {
		const Vec2 &begin = m_outline_pts[0];
		const Vec2 &end = m_outline_pts[4];
		DrawLine(win, begin, end);
	}
}


// Bullet ctor from values. Just prime the bbox with dummy values.
Bullet::Bullet(const Vec2 &position, const Vec2 &momentum) : m_bbox(1.0, Vec2(0.0f, 0.0f)) {
	m_alive = true;
	m_frames_left = BULLET_LIFETIME_FRAMES;
	m_momentum = momentum;
	m_bbox = BoundingBox(BULLET_RADIUS, position);
}


// Copy ctor.
Bullet::Bullet(const Bullet &that) : m_bbox(that.m_bbox) {
	m_alive = that.m_alive;
	m_frames_left = that.m_frames_left;
	m_momentum = that.m_momentum;
	m_bbox = that.m_bbox;
}


// Age the bullet a bit, and apply the momentum to the bounding box.
void Bullet::update() {
	m_frames_left--;
	m_bbox.applyMomentum(m_momentum);
}


// Asteroid ctor from values.
// Just prime the bbox with dummy values.
Asteroid::Asteroid(Game *pGame, ASTEROID_SIZE size, const Vec2 &position, const Vec2 &momentum) :
m_bbox(1.0f, Vec2(0.0f, 0.0f)) {
	m_alive = true;

	float radius;
	switch (size) {
	case AST_SIZE_SMALL:  radius = AST_RADIUS_SMALL;  break;
	case AST_SIZE_MEDIUM: radius = AST_RADIUS_MEDIUM; break;
	case AST_SIZE_LARGE:  radius = AST_RADIUS_LARGE;  break;
	default: radius = AST_RADIUS_SMALL; break; // Should never happen.
	}

	m_size = size;
	m_momentum = momentum;
	m_bbox = BoundingBox(radius, position);
}


// Game default ctor.
Game::Game(const sf::Font &font) : 
	m_ship(this) {

	// Create a text object for the score in the upper left.
	m_score_text = sf::Text("hello", font);
	m_score_text.setCharacterSize(32);
	m_score_text.setStyle(sf::Text::Bold);
	m_score_text.setColor(sf::Color::White);
	m_score_text.setPosition(10, 10);

	// Create a text objbect for in the middle.
	m_big_text = sf::Text("hello", font);
	m_big_text.setCharacterSize(64);
	m_big_text.setStyle(sf::Text::Bold);
	m_big_text.setColor(sf::Color::White);

    // Set all the initial state.
	m_frames_until_next_shot = 0;
	m_flame_pressed = false;
	m_shoot_pressed = false;
	m_rot_cw_pressed = false;
	m_rot_ccw_pressed = false;

	m_score = 0;
	m_lives_left = 3;
	
	m_play_state = PLAY_STATE::NORMAL;
	m_special_timer = 0;

	spawnAllAsteroids();

	// Set up the two outlines showing lives left.
	m_spare_one[0] = m_spare_two[0] = Vec2(0, 0);
	m_spare_one[1] = m_spare_two[1] = Vec2(-SHIP_RADIUS, SHIP_RADIUS);
	m_spare_one[2] = m_spare_two[2] = Vec2(0, -SHIP_RADIUS);
	m_spare_one[3] = m_spare_two[3] = Vec2(SHIP_RADIUS, SHIP_RADIUS);

	float xoffset = SHIP_RADIUS + 20;
	float yoffset = SHIP_RADIUS + 20;
	for (int i = 0; i < 4; i++) {
		m_spare_one[i] = Vec2::Add(m_spare_one[i], Vec2(g_screen_width - xoffset, yoffset));
		m_spare_two[i] = Vec2::Add(m_spare_two[i], Vec2(g_screen_width - (xoffset * 2), yoffset));
	}
}


// Spawn all the asteroids at the start of a level.
void Game::spawnAllAsteroids() {
	for (int i = 0; i < ASTEROID_INITIAL_COUNT; i++) {
		int either = rand() % 2;

		// Left edge.
		if (either == 0) {
			float y = static_cast<float>(rand() % g_screen_width);
			Vec2 position(0, y);
			float deg = static_cast<float>(rand() % 360);
			Vec2 momentum = Vec2::Rotate(Vec2(AST_SPEED_PER_FRAME_LARGE, 0), deg);
			m_asteroids.emplace_back(this, AST_SIZE_LARGE, position, momentum);
		}
		// Top edge.
		else {
			float x = static_cast<float>(rand() % g_screen_height);
			Vec2 position(x, 0);
			float deg = static_cast<float>(rand() % 360);
			Vec2 momentum = Vec2::Rotate(Vec2(AST_SPEED_PER_FRAME_LARGE, 0), deg);
			m_asteroids.emplace_back(this, AST_SIZE_LARGE, position, momentum);
		}
	}
}


// Spawn a bullet.
void Game::spawnBullet() {
	Vec2 bullet_position = m_ship.getTipPoint();
	Vec2 ship_direction  = m_ship.getTipDir();
	Vec2 ship_momentum   = m_ship.getMomentum();

	Vec2 bullet_momentum = Vec2::Add(ship_momentum, Vec2::Multiply(ship_direction, BULLET_SPEED_PER_FRAME));

	m_bullets.emplace_back(bullet_position, bullet_momentum);
	while (m_bullets.size() > BULLET_MAX_COUNT) {
		m_bullets.erase(m_bullets.begin());
	}
}



void Game::update() {
	if (m_play_state == PLAY_STATE::NORMAL) {
		updateNormal();
	}
	else {
		updateSpecial();
	}
}


// Update the game in normal state
// TODO: You have a lot of logic in here. Ponder breaking this out a bit.
void Game::updateNormal() {
	// Update the bullets.
	for (auto it = m_bullets.begin(); it != m_bullets.end(); it++) {
		it->update();
	}

	// Delete any expired bullets. Note the erase-remove idiom here.
	m_bullets.erase(std::remove_if(
		m_bullets.begin(), m_bullets.end(),
		[](Bullet &b) { return b.isExpired(); }),
		m_bullets.end());

	// Update the asteroids.
	for (auto it = m_asteroids.begin(); it != m_asteroids.end(); it++) {
		it->update();
	}

	// The the ship.
	m_ship.update();

	// Finally, spawn any new bullets.
	if (m_frames_until_next_shot > 0) {
		m_frames_until_next_shot--;
	}

	if ((m_frames_until_next_shot == 0) && m_shoot_pressed) {
		spawnBullet();
		m_frames_until_next_shot = SHIP_FRAMES_BETWEEN_SHOTS;
	}

	// Test each bullet against each asteroid.
	std::vector<Asteroid> babies;

	for (auto b_iter = m_bullets.begin(); b_iter != m_bullets.end(); b_iter++) {
		for (auto a_iter = m_asteroids.begin(); a_iter != m_asteroids.end(); a_iter++) {
			Bullet &bullet = *b_iter;
			Asteroid &asteroid = *a_iter;

			// If they collide, kill them both, and update the score.
			// As in, the harder you play, the better your score?
			if (bullet.getBbox().intersectionTest(asteroid.getBbox())) {
				bullet.markAsDead();
				asteroid.markAsDead();
				switch (asteroid.getSize()) {
				case AST_SIZE_SMALL:  addToScore(5); break;
				case AST_SIZE_MEDIUM: addToScore(2); break;
				case AST_SIZE_LARGE:  addToScore(1); break;
				default: break; // Should never happen.
				}

				spawnBabyAsteroids(bullet, asteroid, &babies);
			}
		}
	}

	// Delete any dead bullets.
	m_bullets.erase(std::remove_if(
		m_bullets.begin(), m_bullets.end(),
		[](Bullet &b) { return !b.isAlive(); }),
		m_bullets.end());

	// Delte any dead asteroids.
	m_asteroids.erase(std::remove_if(
		m_asteroids.begin(), m_asteroids.end(),
		[](Asteroid &a) { return !a.isAlive(); }),
		m_asteroids.end());

	// Then, add the newly-spawned babies.
	m_asteroids.insert(m_asteroids.begin(), babies.begin(), babies.end());

	// Do any collisions between asteroids.
	for (auto ita = m_asteroids.begin(); ita != m_asteroids.end(); ita++) {
		for (auto itb = ita + 1; itb != m_asteroids.end(); itb++) {
			if (ita->getBbox().intersectionTest(itb->getBbox())) {
				bounceAsteroids(*ita, *itb);
			}
		}
	}

	// Finally, see if the ship hit an asteroid.
	bool catastrophe = false;
	for (auto ita = m_asteroids.begin(); ita != m_asteroids.end(); ita++) {
		if (m_ship.getBBox().intersectionTest(ita->getBbox())) {
			catastrophe = true;
		}
	}

	if (catastrophe) {
		handleShipDeath();
	}
}


// Update the game, but after a ship death or game over.
void Game::updateSpecial() {
	// Update the asteroids.
	for (auto it = m_asteroids.begin(); it != m_asteroids.end(); it++) {
		it->update();
	}

	// Do any collisions between asteroids.
	for (auto ita = m_asteroids.begin(); ita != m_asteroids.end(); ita++) {
		for (auto itb = ita + 1; itb != m_asteroids.end(); itb++) {
			if (ita->getBbox().intersectionTest(itb->getBbox())) {
				bounceAsteroids(*ita, *itb);
			}
		}
	}
}


// Process when the ship dies.
void Game::handleShipDeath() {
	// Kill off all the bullets.
	m_bullets.erase(m_bullets.begin(), m_bullets.end());

	// Reset the ship's internals.
	m_rot_cw_pressed  = false;
	m_rot_ccw_pressed = false;
	m_shoot_pressed   = false;
	m_flame_pressed   = false;
	m_ship.resetToCenter();

	m_lives_left--;
	if (m_lives_left == 0) {
		m_play_state = PLAY_STATE::GAME_OVER;
		m_frames_until_next_shot = 0;
		m_special_timer = 5000;
	}
	else {
		m_play_state = PLAY_STATE::SHIP_DEATH;
		m_frames_until_next_shot = 0;
		m_special_timer = 5000;
	}

	// TODO: CONTINUE HERE.
}




// We've had a bullet-asteroid collision. Figure out if we need to spawn new ones.
void Game::spawnBabyAsteroids(const Bullet &bullet, const Asteroid &asteroid, 
	                          std::vector<Asteroid> *results) {
	ASTEROID_SIZE new_size;
	float new_speed;

	switch (asteroid.getSize()) {
	case AST_SIZE_LARGE:  
		new_size  = AST_SIZE_MEDIUM; 
		new_speed = AST_SPEED_PER_FRAME_MEDIUM;
		break;
	case AST_SIZE_MEDIUM: 
		new_size  = AST_SIZE_SMALL;
		new_speed = AST_SPEED_PER_FRAME_SMALL;
		break;

	case AST_SIZE_SMALL: return; // Nothing to do.
	default: return; // This should never happen.
	}

	// Create four new ones. Base the center off of four quadrants.
	float half = asteroid.getBbox().getRadius() / 2.0f;
	Vec2 center = asteroid.getBbox().getCenter();

	Vec2 center_v1(center.x + half, center.y + half);
	Vec2 center_v2(center.x + half, center.y - half);
	Vec2 center_v3(center.x - half, center.y + half);
	Vec2 center_v4(center.x - half, center.y - half);

	// Base the momentum on the bullet trajectory, in thirty degree increments.
	Vec2 momentum_base = Vec2::Multiply(bullet.getMomentum().getUnit(), new_speed);
	
	Vec2 momentum_v1 = Vec2::Rotate(momentum_base, -15);
	Vec2 momentum_v2 = Vec2::Rotate(momentum_base, -30);
	Vec2 momentum_v3 = Vec2::Rotate(momentum_base, 15);
	Vec2 momentum_v4 = Vec2::Rotate(momentum_base, 30);

	// And, make 'em.
	results->emplace_back(this, new_size, center_v1, momentum_v1);
	results->emplace_back(this, new_size, center_v2, momentum_v2);
	results->emplace_back(this, new_size, center_v3, momentum_v3);
	results->emplace_back(this, new_size, center_v4, momentum_v4);
}


// For now, just going with a real simple bounce algorithm.
// 1) Find the midway point of the two centers.
// 2) Push both asteroids directly away from that point.
// TODO: A more graceful way would be to bounce them off the "mirror" plane between the two.
void Game::bounceAsteroids(Asteroid &one, Asteroid &two) {
	// Find the midpoint between the centers.
	const Vec2 &ctr1 = one.getBbox().getCenter();
	const Vec2 &ctr2 = two.getBbox().getCenter();
	float midx = (ctr1.x + ctr2.x) / 2.0f;
	float midy = (ctr1.y + ctr2.y) / 2.0f;

	// Push them away from that point.
	Vec2 ray1 = Vec2(ctr1.x - midx, ctr1.y - midy).getUnit();
	Vec2 ray2 = Vec2(ctr2.x - midx, ctr2.y - midy).getUnit();

	one.setMomentum(Vec2::Multiply(ray1, one.getMomentum().getLength()));
	two.setMomentum(Vec2::Multiply(ray2, two.getMomentum().getLength()));
}


void Game::draw(sf::RenderWindow &win) {
	switch (m_play_state) {
	case NORMAL:     drawNormal(win); break;
	case SHIP_DEATH: drawShipDeath(win); break;
	case GAME_OVER:  drawGameOver(win); break;
	default: break; // Should never happen.
	}
}


// Draw the screen for normal game play.
void Game::drawNormal(sf::RenderWindow &win) {
	m_ship.draw(win);

	for (auto it = m_asteroids.begin(); it != m_asteroids.end(); it++) {
		it->draw(win);
	}

	for (auto it = m_bullets.begin(); it != m_bullets.end(); it++) {
		it->draw(win);
	}

	drawLivesLeft(win);
	drawScoreText(win);
}


// Draw the screen after a ship died.
void Game::drawShipDeath(sf::RenderWindow &win) {
	for (auto it = m_asteroids.begin(); it != m_asteroids.end(); it++) {
		it->draw(win);
	}

	drawLivesLeft(win);
	drawScoreText(win);

	char msg[32];
	sprintf(msg, "Lives Left: %d", m_lives_left);
	drawBigText(win, msg);
}


// Draw the "Game Over" screen.
void Game::drawGameOver(sf::RenderWindow &win) {
	for (auto it = m_asteroids.begin(); it != m_asteroids.end(); it++) {
		it->draw(win);
	}

	drawScoreText(win);
	drawBigText(win, "Game Over!");
}


// Draw how many lives are left.
void Game::drawLivesLeft(sf::RenderWindow &win) {
	if (m_lives_left >= 1) {
		for (int i = 0; i < 4; i++) {
			DrawLine(win, m_spare_one[i], m_spare_one[(i + 1) % 4]);
		}
	}

	if (m_lives_left >= 2) {
		for (int i = 0; i < 4; i++) {
			DrawLine(win, m_spare_two[i], m_spare_two[(i + 1) % 4]);
		}
	}
}


// Draw the score text.
void Game::drawScoreText(sf::RenderWindow &win) {
	char msg[64];
	sprintf(msg, "Score: %d", m_score);
	m_score_text.setString(msg);
	win.draw(m_score_text);
}


// Draw big text in the center of the screen.
void Game::drawBigText(sf::RenderWindow &win, const char *msg) {
	m_big_text.setString(msg);

	sf::FloatRect bounds = m_big_text.getLocalBounds();
	float origin_x = bounds.left + (bounds.width / 2.0f);
	float origin_y = bounds.top + (bounds.height / 2.0f);
	m_big_text.setOrigin(origin_x, origin_y);

	float screen_x = g_screen_width  / 2.0f;
	float screen_y = g_screen_height / 2.0f;
	m_big_text.setPosition(screen_x, screen_y);

	win.draw(m_big_text);
}


// Handle a key being pressed down.
// Return true if we're meant for the game to continue.
void Game::onKeyPressed(sf::Keyboard::Key code) {

	switch (m_play_state) {
	// Normal play, use the keys to fly the ship.
	case NORMAL:
		switch (code) {
		case sf::Keyboard::Space: m_shoot_pressed = true;  break;
		case sf::Keyboard::W: m_flame_pressed = true; break;
		case sf::Keyboard::D: m_rot_cw_pressed = true; break;
		case sf::Keyboard::A: m_rot_ccw_pressed = true; break;
		default: break;
		}
		break;

	// After the ship dies, space will respawn.
	case SHIP_DEATH:
		if (code == sf::Keyboard::Space) {
			m_play_state = NORMAL;
		}
		break;

	// Once the game is over, nothing to do.
	case GAME_OVER:
		break;
	}
}


// Handle a key being released.
// Only applicable during normal gameplay.
void Game::onKeyReleased(sf::Keyboard::Key code) {
	if (m_play_state == NORMAL) {
		switch (code) {
		case sf::Keyboard::Space: m_shoot_pressed = false; break;
		case sf::Keyboard::W: m_flame_pressed = false; break;
		case sf::Keyboard::D: m_rot_cw_pressed = false; break;
		case sf::Keyboard::A: m_rot_ccw_pressed = false; break;
		default: break;
		}
	}
}


// And away we go.
int __stdcall WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR pCmdLine, int nCmdShow)
{
	// Open the window.
	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	g_screen_width  = desktop.width;
	g_screen_height = desktop.height;

	sf::RenderWindow window(desktop, "Minimalist Asteroids", sf::Style::Fullscreen);
	window.setMouseCursorVisible(false);

	// Declare and load a font.
	// TODO: For now, shamelessly hard-code the path to a system font.
	sf::Font font;
	if (!font.loadFromFile("C:\\Windows\\Fonts\\arial.ttf")) {
		OutputDebugStringA("Could not load font C:\\Windows\\Fonts\\arial.ttf");
		return 1;
	}

	Game game(font);

	sf::Clock timer;
	sf::Int32 start_msecs = timer.getElapsedTime().asMilliseconds();
	sf::Int32 prev_msecs  = start_msecs;

	int frame_num = 0;
	while (window.isOpen()) {
		// Deal with events.
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			else if (event.type == sf::Event::KeyPressed) {
				sf::Keyboard::Key code = event.key.code;
				if (code == sf::Keyboard::Escape) {
					window.close();
				}
				else {
					game.onKeyPressed(event.key.code);
				}
			}
			else if (event.type == sf::Event::KeyReleased) {
				game.onKeyReleased(event.key.code);
			}
		}

		// Our simple game loop.
		sf::Int32 curr_msecs = timer.getElapsedTime().asMilliseconds();
		if (curr_msecs >= (prev_msecs + MSECS_PER_FRAME)) {
			window.clear();
			game.update();
			game.draw(window);
			window.display();

			prev_msecs += MSECS_PER_FRAME;
			frame_num++;
		}
	}

	// All done!
	window.setMouseCursorVisible(true);
	return 0;
}
