Minimalist Asteroids
====================

Here's a simple game programming challenge I read about a couple of years ago,
which a games studio used as an interview exercise. The challenge was to sit in
a room, with a PC not connected to the Internet, and within a couple of hours,
write the classic arcade game "Asteroids" from scratch, using just three system
calls:

1. Clear the screen.
2. Draw a line between two points.
3. Get the system time.

For simplicity, the asteroids were allowed to be simple squares. Otherwise, it
didn't have to be pretty, it just had to work. Needless to say, the candidate
had to be intimately familiar with the basics of translations, rotations, and
collision detection to get the project done.

This has taken me a bit longer than two hours, ;) but it's been fun to do, and
a good refresher course.
